        <?php
            function copyAndReplaceText($routeFile, $routeCopiedFile, $word, $newWord) {
                if(file_exists($routeFile)) {
                    $fileContent = file_get_contents($routeFile);

                    $copiedFileContent = file_get_contents($routeCopiedFile);

                    $replacedText = str_replace($word, $newWord, $fileContent);

                    copy($routeFile, $routeCopiedFile);

                    $transcriptor = fopen($routeCopiedFile, 'w');

                    fwrite($transcriptor, $replacedText);

                    fclose($transcriptor);

                    echo $copiedFileContent;
                }
            }

            copyAndReplaceText('quijote.txt', 'quijote-copia.txt', 'Sancho', 'Morty');
        ?>