<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Empezando a trabajar con ficheros en PHP</title>
    </head>
    
    <body>
        <h1>Empezando a trabajar con ficheros en PHP</h1>

        <?php
            function wordSearcher($word) {
                $descriptor = fopen('quijote.txt', 'r');

                $wordNumber = 0;

                while (($contenido = fgets($descriptor)) !== false) { 
                    $arrayText = explode(' ', $contenido);

                    if(in_array($word, $arrayText)) {
                        $wordNumber ++;
                    }
                }

                echo "la palabra $word aparece $wordNumber veces";
                
                fclose($descriptor);
            }

            wordSearcher('molino');
        ?>
    </body>
</html>