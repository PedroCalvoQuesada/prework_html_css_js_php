<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mi primer script en php</title>
    </head>
    
    <body>
        <h1>Mi primer script en PHP</h1>

        <?php 
            function whatDayItIs($number) {
                $week = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

                foreach ($week as $weekDayArrayNumber => $weekDay){
                    if(($number - 1) == $weekDayArrayNumber) {
                        echo $weekDay;
                    }
                }
            }

            whatDayItIs(1);

            
        ?>
    </body>
</html>