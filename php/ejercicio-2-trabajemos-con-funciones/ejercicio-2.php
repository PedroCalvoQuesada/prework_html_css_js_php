<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Trabajando con funciones en PHP</title>
    </head>

    <body>
        <h1>Trabajando con funciones en PHP</h1>

        <form action="ejercicio-2.php" method = "POST">
            <label for="text">
                <h4>Comprueba si tu texto contiene las 5 vocales</h4>
                <input type="text" name="text" placeholder="Introduce un texto">
            </label>

            <input type="submit" value="Comprobar texto">
        </form>

        <?php
            function fiveVocalsComparator($text) {
                $textArray = str_split($text);
                
                if(in_array('a', $textArray) && in_array('e', $textArray) && in_array('i', $textArray) && in_array('o', $textArray) && in_array('u', $textArray)) {
                    echo 'El texto contiene las 5 vocales';
                } else {
                    echo 'El texto NO contiene las 5 vocales';
                }
            }

            fiveVocalsComparator($_POST['text']);
        ?>
    </body>
</html>