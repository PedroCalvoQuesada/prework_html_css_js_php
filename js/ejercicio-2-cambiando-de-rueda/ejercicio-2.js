function wheelComparator(diameter) {
    if (diameter <= 10) {
        console.log("Es una rueda para juguetes pequeños");
    }

    if (diameter > 10 && diameter < 20) {
        console.log("Es una rueda para juguetes medianos");
    }

    if (diameter >= 20) {
        console.log("Es una rueda para juguetes grandes");
    }
}

wheelComparator(5);
wheelComparator(15);
wheelComparator(25);
